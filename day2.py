#!/usr/bin/env python

# https://adventofcode.com/2021/day/2

input_filename = "input2.txt"
# input_filename = "sample2.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

hpos = 0
depth = 0

for d, x in (i.split(' ') for i in data):
    x = int(x)
    if d == 'forward':
        hpos += x
    if d == 'down':
        depth += x
    if d == 'up':
        depth -= x

print("part1:", hpos * depth)

hpos = 0
depth = 0
aim = 0

for d, x in (i.split(' ') for i in data):
    x = int(x)
    if d == 'forward':
        hpos += x
        depth += aim * x
    if d == 'down':
        aim += x
    if d == 'up':
        aim -= x

print("part2:", hpos * depth)
