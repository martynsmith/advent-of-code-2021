#!/usr/bin/env python

# https://adventofcode.com/2021/day/5

from typing import List, Iterable, NamedTuple
from rich import print
from aoc import Vector
import re

input_filename = "input5.txt"
# input_filename = "sample5.txt"


class Line(NamedTuple):
    start: Vector
    end: Vector

    def is_hv(self) -> bool:
        return self.start.x == self.end.x or self.start.y == self.end.y

    def vectors(self) -> Iterable[Vector]:
        pos = self.start
        delta = Vector(
            max(-1, min(1, self.end.x - self.start.x)),
            max(-1, min(1, self.end.y - self.start.y))
        )

        while pos != self.end:
            yield pos
            pos += delta

        yield pos


lines = []
with open(input_filename) as fh:
    for line in fh.readlines():
        match = re.search(r'^(\d+),(\d+) -> (\d+),(\d+)$', line)
        if not match:
            raise Exception()

        lines.append(Line(
            Vector(int(match.group(1)), int(match.group(2))),
            Vector(int(match.group(3)), int(match.group(4))),
        ))

hv_lines = [l for l in lines if l.is_hv()]

grid = {}
for line in hv_lines:
    for v in line.vectors():
        grid.setdefault(v, 0)
        grid[v] += 1

print("part1:", len([v for v in grid.values() if v > 1]))

grid = {}
for line in lines:
    for v in line.vectors():
        grid.setdefault(v, 0)
        grid[v] += 1

print("part2:", len([v for v in grid.values() if v > 1]))
