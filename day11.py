#!/usr/bin/env python

# https://adventofcode.com/2021/day/11

from rich import print
from aoc import Vector

input_filename = "input11.txt"
# input_filename = "sample11.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

grid = {}

neighbours = [
    Vector(-1, -1),
    Vector(0, -1),
    Vector(1, -1),
    Vector(-1, 0),
    Vector(0, 0),
    Vector(1, 0),
    Vector(-1, 1),
    Vector(0, 1),
    Vector(1, 1),
]

for y, line in enumerate(data):
    for x, char in enumerate(line):
        grid[Vector(x, y)] = int(char)


def step() -> int:
    prev_flashes = None
    flashes = 0

    for pos in list(grid.keys()):
        grid[pos] += 1

    while prev_flashes != flashes:
        prev_flashes = flashes
        for pos, value in list(grid.items()):
            if grid[pos] > 9 and grid[pos] != 99999:
                grid[pos] = 99999
                flashes += 1
                for n in neighbours:
                    if pos + n not in grid:
                        continue
                    if grid[pos + n] == 99999:
                        continue
                    grid[pos + n] += 1

    for pos in list(grid.keys()):
        if grid[pos] > 9:
            grid[pos] = 0

    return flashes


total_flashes = 0
for i in range(100):
    total_flashes += step()

print("part1:", total_flashes)

step_count = 100
while set(grid.values()) != set([0]):
    step()
    step_count += 1

print("part2:", step_count)
