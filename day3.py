#!/usr/bin/env python

# https://adventofcode.com/2021/day/3

input_filename = "input3.txt"
# input_filename = "sample3.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

def get_most_common(lines):
    registers = [0 for b in lines[0]]
    for line in lines:
        for i, bit in enumerate(line):
            if bit == "0":
                registers[i] -= 1
            elif bit == "1":
                registers[i] += 1
    return "".join(["0" if b < 0 else "1" for b in registers])

def invert(n):
    return n.translate(str.maketrans("01", "10"))

balance = get_most_common(data)

gamma_rate = int(balance, base=2)
epsilon_rate = int(invert(balance), base=2)

print("part1:", gamma_rate * epsilon_rate)

oxygen_list = data
co2_list = data

for i in range(len(oxygen_list[0])):
    if len(oxygen_list) > 1:
        balance = get_most_common(oxygen_list)
        oxygen_list = [n for n in oxygen_list if n[i] == balance[i]]
    if len(co2_list) > 1:
        balance = get_most_common(co2_list)
        co2_list = [n for n in co2_list if n[i] != balance[i]]

oxygen_rating = int(oxygen_list[0], base=2)
co2_rating = int(co2_list[0], base=2)

print("part2:", oxygen_rating * co2_rating)
