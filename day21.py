#!/usr/bin/env python

# https://adventofcode.com/2021/day/21

from typing import NamedTuple
from rich import print
from itertools import cycle, islice, combinations
import attr
from functools import cache

input_filename = "input21.txt"
# input_filename = "sample21.txt"


@attr.define
class Player:
    id: int
    position: int
    score: int


with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

p1 = Player(id=1, position=int(data[0].split(':')[1]), score=0)
p2 = Player(id=2, position=int(data[1].split(':')[1]), score=0)

die = cycle(i + 1 for i in range(100))
roll_count = 0


def move(player, d):
    global roll_count
    roll = sum(islice(d, 3))
    roll_count += 3
    player.position = (player.position - 1 + roll) % 10 + 1
    player.score += player.position


players = cycle([p1, p2])

for player in players:
    move(player, die)
    if player.score >= 1000:
        break

loser = next(players)
print("part1:", loser.score * roll_count)


class State(NamedTuple):
    to_play: int
    p1_pos: int
    p1_score: int
    p2_pos: int
    p2_score: int


initial_state = State(
    to_play=1,
    p1_pos=int(data[0].split(':')[1]),
    p1_score=0,
    p2_pos=int(data[1].split(':')[1]),
    p2_score=0,
)

rolls = {}
for roll in combinations((1, 2, 3) * 3, 3):
    rolls.setdefault(sum(roll), 0)
    rolls[sum(roll)] += 1

universes = {
    initial_state: 1
}

wins = {
    1: 0,
    2: 0,
}


def dirac_move(state: State):
    print(state.p1_score, state.p2_score)
    for roll, count in rolls.items():
        if state.to_play == 1:
            new_pos = ((state.p1_pos - 1) + roll) % 10 + 1
            new_state = State(
                to_play=2,
                p1_pos=new_pos,
                p1_score=state.p1_score + new_pos,
                p2_pos=state.p2_pos,
                p2_score=state.p2_score,
            )
            if new_state.p1_score >= 21:
                wins[1] += universes[state] * count
                print(wins)
                continue
        elif state.to_play == 2:
            new_pos = ((state.p2_pos - 1) + roll) % 10 + 1
            new_state = State(
                to_play=1,
                p1_pos=state.p1_pos,
                p1_score=state.p1_score,
                p2_pos=new_pos,
                p2_score=state.p2_score + new_pos,
            )
            if new_state.p2_score >= 21:
                wins[1] += universes[state] * count
                print(wins)
                continue
        else:
            raise Exception()

        universes.setdefault(new_state, 0)
        universes[new_state] += count * universes[state]
    universes.pop(state)


# while universes:
#     # print(universes)
#     state_to_move = sorted(universes.keys(), key=lambda s: s.p1_score + s.p2_score)[0]
#     dirac_move(state_to_move)
#
# print(universes, wins)

# ---------------------------

@cache
def new_position(position, roll):
    return ((position - 1) + roll) % 10 + 1

@cache
def win_count(positions, scores, turn=0):
    wins = [0, 0]

    # for roll, frequency in rolls.items():
    for r1 in range(1, 4):
        for r2 in range(1, 4):
            for r3 in range(1, 4):
                roll = r1 + r2 + r3
                new_positions = list(positions)
                new_scores = list(scores)
                new_positions[turn] = new_position(new_positions[turn], roll)
                new_scores[turn] += new_positions[turn]
                if new_scores[turn] >= 21:
                    wins[turn] += 1
                else:
                    sub_wins = list(win_count(tuple(new_positions), tuple(new_scores), 1 - turn))
                    # sub_wins[turn] *= frequency
                    wins[0] += sub_wins[0]
                    wins[1] += sub_wins[1]

    return tuple(wins)


print(win_count(
    positions=(
        int(data[0].split(':')[1]),
        int(data[1].split(':')[1]),
    ),
    scores=(0, 0),
))
