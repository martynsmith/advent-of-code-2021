#!/usr/bin/env python

# https://adventofcode.com/2021/day/25

from aoc import Vector, render_grid

input_filename = "input25.txt"
# input_filename = "sample25.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

grid = {}

for y, line in enumerate(data):
    if not line:
        continue
    max_y = y
    for x, cucumber in enumerate(line):
        max_x = x
        if cucumber == '.':
            continue
        grid[Vector(x, y)] = cucumber


def new_position(cucumber, position):
    if cucumber == '>':
        if position.x == max_x:
            return Vector(0, position.y)
        else:
            return Vector(position.x + 1, position.y)
    elif cucumber == 'v':
        if position.y == max_y:
            return Vector(position.x, 0)
        else:
            return Vector(position.x, position.y + 1)
    else:
        raise Exception()


x_range = range(0, max_x + 1)
y_range = range(0, max_y + 1)


def step():
    for herd in ('>', 'v'):
        moves = []
        for v in sorted((v for v, c in grid.items() if c == herd), key=lambda v: (v.y, v.x)):
            nv = new_position(herd, v)
            if nv not in grid:
                moves.append((v, nv))
        for v, nv in moves:
            grid[nv] = grid.pop(v)


old_positions = set()
steps = 0
while True:
    positions = set(i for i in grid.items())
    if positions == old_positions:
        break
    old_positions = positions
    steps += 1
    step()

print("part 1:", steps)
