#!/usr/bin/env python

# https://adventofcode.com/2021/day/20
from typing import Set, Dict

import attr
from rich import print
from aoc import Vector, render_grid

input_filename = "input20.txt"
# input_filename = "sample20.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

algorithm = data.pop(0)
algorithm = [1 if c == '#' else 0 for c in algorithm]
assert len(algorithm) == 512
data.pop(0)

grid = set()


def generate_candidates(v):
    return [
        v + Vector(-1, -1),
        v + Vector(0, -1),
        v + Vector(1, -1),
        v + Vector(-1, 0),
        v,
        v + Vector(1, 0),
        v + Vector(-1, 1),
        v + Vector(0, 1),
        v + Vector(1, 1),
    ]


def get_index(v, grid):
    b = ""
    for y in range(v.y - 1, v.y + 2):
        for x in range(v.x - 1, v.x + 2):
            b += str(grid.points.get(Vector(x, y), grid.default))

    return int(b, 2)


@attr.define
class Grid:
    points: Dict[Vector, int]
    default: int

    def get_bounds(self):
        min_x = min(v.x for v in self.points.keys())
        max_x = max(v.x for v in self.points.keys())
        min_y = min(v.y for v in self.points.keys())
        max_y = max(v.y for v in self.points.keys())

        return min_x, max_x, min_y, max_y



def enhance(grid):
    new_grid = Grid(
        points={},
        default=algorithm[int(str(grid.default) * 9, 2)]
    )

    min_x, max_x, min_y, max_y = grid.get_bounds()

    for y in range(min_y-1, max_y+2):
        for x in range(min_x-1, max_x+2):
            v = Vector(x, y)
            new_grid.points[v] = algorithm[get_index(v, grid)]

    return new_grid


grid = Grid(points={}, default=0)
for y, row in enumerate(data):
    for x, cell in enumerate(row):
        if cell == "#":
            grid.points[Vector(x, y)] = 1
        elif cell == ".":
            grid.points[Vector(x, y)] = 0
        else:
            raise Exception()

for i in range(2):
    # print(render_grid({v: [".", "#"][c] for v, c in grid.points.items()}))
    grid = enhance(grid)

# print(render_grid({v: [".", "#"][c] for v, c in grid.points.items()}))

# 5222 is too low
print("part1:", len([c for c in grid.points.values() if c]))

for i in range(48):
    # print(render_grid({v: [".", "#"][c] for v, c in grid.points.items()}))
    grid = enhance(grid)

print("part2:", len([c for c in grid.points.values() if c]))
