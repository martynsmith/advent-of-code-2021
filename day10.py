#!/usr/bin/env python

# https://adventofcode.com/2021/day/10

from typing import Tuple

input_filename = "input10.txt"
# input_filename = "sample10.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

closing_for = {
    "(": ")",
    "[": "]",
    "{": "}",
    "<": ">",
}

corruption_score_for = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137,
}

complete_score_for = {
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4,
}


def get_syntax_score(string):
    stack = []

    for char in string:
        if char in closing_for:
            stack.append(char)
        else:
            if not stack:
                return "incomplete"
            expected_close = closing_for[stack.pop()]
            if char != expected_close:
                return corruption_score_for[char]

    return 0


print("part1:", sum(get_syntax_score(l) for l in data))

data = [l for l in data if get_syntax_score(l) == 0]


def get_completion_score(string):
    stack = []

    for char in string:
        if char in closing_for:
            stack.append(char)
        else:
            if not stack:
                raise Exception("nope")
            expected_close = closing_for[stack.pop()]
            if char != expected_close:
                raise Exception("corrupt line")
    score = 0
    for char in [closing_for[c] for c in reversed(stack)]:
        score *= 5
        score += complete_score_for[char]

    return score


scores = sorted([get_completion_score(l) for l in data])

print("part2:", scores[len(scores) // 2])
