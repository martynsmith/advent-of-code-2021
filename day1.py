#!/usr/bin/env python

# https://adventofcode.com/2021/day/1

input_filename = "input1.txt"
# input_filename = "sample1.txt"

with open(input_filename) as fh:
    data = [int(l.strip()) for l in fh.readlines()]

increases = 0
for i in range(0, len(data)-1):
    if data[i+1] > data[i]:
        increases += 1

print("part1:", increases)

increases = 0
for i in range(0, len(data)-2):
    if sum(data[i+1:i+4]) > sum(data[i:i+3]):
        increases += 1

print("part2:", increases)
