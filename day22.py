#!/usr/bin/env python

# https://adventofcode.com/2021/day/22

from typing import NamedTuple
from rich import print
import re


class Cube(NamedTuple):
    x1: int
    x2: int
    y1: int
    y2: int
    z1: int
    z2: int
    effect: int

    @property
    def volume(self):
        return (self.x2 - self.x1 + 1) * (self.y2 - self.y1 + 1) * (self.z2 - self.z1 + 1)

    def get_intersection(self, other, effect):
        c = Cube(
            max(self.x1, other.x1),
            min(self.x2, other.x2),
            max(self.y1, other.y1),
            min(self.y2, other.y2),
            max(self.z1, other.z1),
            min(self.z2, other.z2),
            effect,
        )
        if c.x2 - c.x1 < 0 or c.y2 - c.y1 < 0 or c.z2 - c.z1 < 0:
            return None

        return c


input_filename = "input22.txt"
# input_filename = "sample22.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]


def find_lit_count(bounding_box=None):
    cubes = []

    for line in data:
        op, cube_str = line.split(' ', maxsplit=2)
        points = list(map(int, re.findall(r'(-?\d+)', cube_str)))
        cube = Cube(
            *sorted(points[:2]),
            *sorted(points[2:4]),
            *sorted(points[4:]),
            1 if op == "on" else -1
        )
        if bounding_box:
            cube = bounding_box.get_intersection(cube, cube.effect)
            if not cube:
                continue

        for candidate in list(cubes):
            intersection = candidate.get_intersection(cube, candidate.effect * -1)
            if not intersection:
                continue
            cubes.append(intersection)

        if op == 'on':
            cubes.append(cube)
    return sum([c.volume * c.effect for c in cubes])


print("part 1:", find_lit_count(bounding_box=Cube(-50, 50, -50, 50, -50, 50, 0)))
print("part 2:", find_lit_count())
