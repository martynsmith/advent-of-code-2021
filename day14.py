#!/usr/bin/env python

# https://adventofcode.com/2021/day/14
from collections import defaultdict

from rich import print
import re

input_filename = "input14.txt"
# input_filename = "sample14.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]
    template = data.pop(0)
    data.pop(0)

mapping = {}

for line in data:
    source, target = line.split(' -> ')
    mapping[source] = target

character_counts = defaultdict(int)
pair_counts = defaultdict(int)

for i, char in enumerate(template):
    character_counts[char] += 1
    if i < len(template) - 1:
        pair_counts[template[i:i + 2]] += 1


def step():
    for pair, count in list(pair_counts.items()):
        if pair not in mapping:
            continue
        insert_char = mapping[pair]
        character_counts[insert_char] += count
        pair_counts[pair] -= count
        pair_counts[pair[0] + insert_char] += count
        pair_counts[insert_char + pair[1]] += count


for i in range(10):
    step()

print("part1:", max(character_counts.values()) - min(character_counts.values()))

for i in range(30):
    step()

print("part2:", max(character_counts.values()) - min(character_counts.values()))
