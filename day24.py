#!/usr/bin/env python

# https://adventofcode.com/2021/day/24

from rich import print

input_filename = "input24.txt"
# input_filename = "optimisedinput24.txt"
# input_filename = "sample24.txt"

with open(input_filename) as fh:
    program = [l.strip() for l in fh.readlines()]


def run(program, params):
    registers = dict(
        w=0,
        x=0,
        y=0,
        z=0,
    )

    for line in program:
        op, *args = line.split(' ')

        if op == 'inp':
            registers[args[0]] = params.pop(0)
            print(f"[red]{op}[/red] {14-len(params)}, {args}, {registers}")
            continue

        arg1 = registers[args[1]] if args[1] in registers else int(args[1])

        if op == 'add':
            registers[args[0]] += arg1
        elif op == 'mul':
            registers[args[0]] *= arg1
        elif op == 'div':
            registers[args[0]] //= arg1
        elif op == 'mod':
            registers[args[0]] %= arg1
        elif op == 'eql':
            if args[1] != '0':
                if registers['w'] != registers['x']:
                    print(f"{registers['w']} == {registers['x']}")
            if registers[args[0]] == arg1:
                registers[args[0]] = 1
            else:
                registers[args[0]] = 0
        else:
            raise Exception(f"Unknown opcode {op}")

        # print(f"{op}, {args}, {registers}")
    return registers['z']

# 1  z*26 + 14
# 2  z*26 + 2
#     3  z*26 + 1
#     4  z*26 + 13
#     5  z*26 + 5
#     6  z%26 - ? == w
#     7  z%26 - ? == w
#     8  z*26 + ?
#     9  z%26 - ? == w
#     10 z*26 + ?
#     11 z%26 - ? == w
#     12 z%26 - ? == w
# 13 z%26 - ? == w
# 14 z%26 - ? == w

# part 1
# ret = run(program, [
#     2,
#     9,
#     9,
#     8,
#     9,
#     2,
#     9,
#     7,  # 8
#     9,  # 9
#     4,  # 10
#     9,  # 11
#     5,  # 12
#     1,  # 13
#     9,  # 14
# ])
# print(ret)

# part 2
ret = run(program, [
    1,
    9,
    5,
    1,
    8,  # 5
    1,  # 6
    2,  # 7
    1,  # 8
    3,  # 9
    1,  # 10
    6,  # 11
    1,  # 12
    1,  # 13
    8,  # 14
])
print(ret)
