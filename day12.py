#!/usr/bin/env python

# https://adventofcode.com/2021/day/12

from collections import defaultdict
from rich import print
from functools import cache

input_filename = "input12.txt"
# input_filename = "sample12.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

routes = defaultdict(set)

for line in data:
    n1, n2 = line.split('-')
    routes[n1].add(n2)
    routes[n2].add(n1)


@cache
def is_small_cave(node):
    return node == node.lower()


def find_path_count(node, visited, double_node, path):
    if node == 'end':
        # print("path:", set([tuple(path + ['end'])]))
        return {tuple(path + ['end'])}

    paths = set()
    for next_node in routes[node]:
        if next_node == double_node:
            if visited[next_node] > 1:
                continue
        else:
            if visited[next_node] > 0:
                continue
        if is_small_cave(node):
            visited[node] += 1
        path.append(node)
        paths.update(find_path_count(next_node, visited, double_node, path))
        path.pop(-1)
        if is_small_cave(node):
            visited[node] -= 1

    return paths


print("part1:", len(find_path_count('start', defaultdict(int), double_node=None, path=[])))

part_two_paths = set()
for node in [n for n in routes.keys() if is_small_cave(n) and n not in ('start', 'end')]:
    part_two_paths.update(find_path_count('start', defaultdict(int), double_node=node, path=[]))
print("part2:", len(part_two_paths))
