#!/usr/bin/env python

# https://adventofcode.com/2021/day/8

from rich import print
from itertools import permutations

input_filename = "input8.txt"
# input_filename = "sample8.txt"

digit_for = {
    "abcefg": 0,
    "cf": 1,
    "acdeg": 2,
    "acdfg": 3,
    "bcdf": 4,
    "abdfg": 5,
    "abdefg": 6,
    "acf": 7,
    "abcdefg": 8,
    "abcdfg": 9,
}

letters = "abcdefg"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

part1_count = 0
part2_sum = 0

potential_maps = [str.maketrans(letters, "".join(potential)) for potential in permutations(letters)]

for line in data:
    patterns, output = [[ss for ss in s.strip().split(' ')] for s in line.strip().split('|')]

    # Part1 (easy)
    for pattern in output:
        if len(pattern) in (2, 4, 3, 7):
            part1_count += 1

    # Part2 (I made a real mess of this before just brute forcing like this)
    for mapping in potential_maps:
        good = True
        for pattern in patterns:
            translated = "".join(sorted(pattern.translate(mapping)))
            if translated not in digit_for:
                good = False
                break

        if good:
            output_list = []
            for pattern in output:
                translated = "".join(sorted(pattern.translate(mapping)))
                output_list.append(digit_for[translated])
            part2_sum += int("".join(str(c) for c in output_list))

print("part1:", part1_count)
print("part2:", part2_sum)
