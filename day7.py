#!/usr/bin/env python

# https://adventofcode.com/2021/day/7

from functools import cache

input_filename = "input7.txt"
# input_filename = "sample7.txt"

with open(input_filename) as fh:
    data = [int(n) for n in fh.readlines()[0].strip().split(',')]

min_fuel = None

for potential_position in range(min(data), max(data) + 1):
    fuel = 0
    for n in data:
        fuel += abs(potential_position - n)
    min_fuel = min(fuel, min_fuel) if min_fuel else fuel

print("part1:", min_fuel)


@cache
def fuel_calc(n):
    total = n
    while n:
        n -= 1
        total += n
    return total


min_fuel = None

for potential_position in range(min(data), max(data) + 1):
    fuel = 0
    for n in data:
        fuel += fuel_calc(abs(potential_position - n))
    min_fuel = min(fuel, min_fuel) if min_fuel else fuel

print("part2:", min_fuel)
