#!/usr/bin/env python

# https://adventofcode.com/2021/day/4

from typing import List
import re
from attr import define
from rich import print

input_filename = "input4.txt"
# input_filename = "sample4.txt"


@define
class Board:
    rows: List[List[int]]
    cols: List[List[int]]

    def call(self, n):
        for i, r in enumerate(self.rows):
            self.rows[i] = ['*' if c == n else c for c in r]
        for i, r in enumerate(self.cols):
            self.cols[i] = ['*' if c == n else c for c in r]

    def is_winner(self):
        for r in self.rows:
            if set(r) == {"*"}:
                return True

        for r in self.cols:
            if set(r) == {"*"}:
                return True

        return False

    def sum_uncalled(self):
        total = 0
        for r in self.rows:
            total += sum(c for c in r if c != '*')
        return total

boards = []

with open(input_filename) as fh:
    draw = [int(n) for n in fh.readline().split(',')]
    board = None
    for line in (l.strip() for l in fh.readlines()):
        if not line:
            if board:
                boards.append(board)
            board = Board(rows=[], cols=[])
            continue
        board.rows.append([int(n) for n in re.split(r'\s+', line)])

if board:
    boards.append(board)

for board in boards:
    board.cols = list(zip(*board.rows))

# print(f"{draw=}")
# print(boards)

first_winner = None
first_winner_called = None
last_winner = None
last_winner_called = None

for n in draw:
    # print("draw:", n)
    for board in list(boards):
        board.call(n)
        if board.is_winner():
            boards.remove(board)
            if not first_winner:
                first_winner = board
                first_winner_called = n
            last_winner = board
            last_winner_called = n

print("part1:", first_winner.sum_uncalled() * first_winner_called)
print("part2:", last_winner.sum_uncalled() * last_winner_called)
