#!/usr/bin/env python

# https://adventofcode.com/2021/day/13

from aoc import Vector, render_grid
from rich import print
import re

input_filename = "input13.txt"
# input_filename = "sample13.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

dots = set()
folds = []

for line in data:
    if not line:
        continue
    if match := re.match('fold along (x|y)=(\d+)', line):
        folds.append((match.group(1), int(match.group(2))))
        continue
    else:
        dots.add(Vector(*map(int, line.split(','))))

part1_done = False

for fold_axis, fold_point in folds:
    for dot in list(dots):
        if fold_axis == "y":
            if dot.y == fold_point:
                dots.discard(dot)
            if dot.y > fold_point:
                dots.discard(dot)
                dots.add(Vector(dot.x, fold_point * 2 - dot.y))
        elif fold_axis == "x":
            if dot.x == fold_point:
                dots.discard(dot)
            if dot.x > fold_point:
                dots.discard(dot)
                dots.add(Vector(fold_point * 2 - dot.x, dot.y))
        else:
            raise Exception()

    if not part1_done:
        print("part1:", len(dots))
        part1_done = True


print("part2:")
print(render_grid({d: '\u2588' for d in dots}))
