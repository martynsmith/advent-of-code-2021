#!/usr/bin/env python

# https://adventofcode.com/2021/day/18

import functools
import itertools
import re
import math
from rich import print

input_filename = "input18.txt"
# input_filename = "sample18.txt"


def reduce(line):
    depth = 0
    for i, n in enumerate(line):
        if n == '[':
            depth += 1
        if n == ']':
            depth -= 1
        if depth == 5:
            head = line[:i]
            tail = line[i:]
            match = re.search(r'\[(\d+),(\d+)]', tail)
            n1, n2 = map(int, match.groups())
            head = re.sub(r'(.*)\b(\d+)', lambda m: m.group(1) + str(int(m.group(2)) + n1), head, count=1)
            tail = re.sub(r'\[(\d+),(\d+)]', '0', tail, count=1)
            tail = re.sub(r'(0.*?)(\d+)', lambda m: m.group(1) + str(int(m.group(2)) + n2), tail, count=1)
            # print("explode:", head+tail)
            return reduce(head + tail)

    split_line = re.sub(
        r'(\d\d+)',
        lambda m: '[' + str(int(m.group(1)) // 2) + ',' + str(math.ceil(int(m.group(1)) / 2)) + ']',
        line,
        count=1
    )
    if split_line != line:
        # print("split:", split_line)
        return reduce(split_line)
    return line


def add(line1, line2):
    return reduce("[" + line1 + "," + line2 + "]")


def get_magnitude(line):
    while '[' in line:
        line = re.sub(r'\[(\d+),(\d+)]', lambda m: str(int(m.group(1)) * 3 + int(m.group(2)) * 2), line)
    return int(line)


with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines() if l[0] != '#']

answer = functools.reduce(add, data)
print("part1:", get_magnitude(answer))

max_magnitude = 0
for line1, line2 in itertools.combinations(data, 2):
    magnitude1 = get_magnitude(add(line1, line2))
    magnitude2 = get_magnitude(add(line2, line1))
    max_magnitude = max(max_magnitude, magnitude1, magnitude2)

print("part2:", max_magnitude)
