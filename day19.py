#!/usr/bin/env python

# https://adventofcode.com/2021/day/19
import re
from typing import NamedTuple, List, Optional
from rich import print
import attr
import itertools

input_filename = "input19.txt"
# input_filename = "sample19.txt"


class Vector(NamedTuple):
    x: int
    y: int
    z: int

    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y, self.z - other.z)

    def __mul__(self, other):
        return Vector(self.x * other.x, self.y * other.y, self.z * other.z)


def generate_simple_transform(t):
    return lambda v: v * t


def generate_complex_transform(shuffle, transform):
    return lambda v: shuffle(v) * transform


transforms = [
    generate_simple_transform(Vector(x, y, z))
    for x, y, z in itertools.product(*[[-1, 1]] * 3)
]

for shuffle in [
    lambda v: Vector(v.x, v.z, v.y),
    lambda v: Vector(v.y, v.x, v.z),
    lambda v: Vector(v.y, v.z, v.x),
    lambda v: Vector(v.z, v.x, v.y),
    lambda v: Vector(v.z, v.y, v.x),
]:
    transforms.extend([
        generate_complex_transform(shuffle, Vector(x, y, z))
        for x, y, z in itertools.product(*[[-1, 1]] * 3)
    ])

s = Vector(3, 4, 5)


@attr.define
class Scanner:
    id: str
    position: Optional[Vector]
    transform: Optional[Vector]
    beacons: List[Vector]

    def manhattan_distance(self, other):
        return abs(self.position.x - other.position.x) + abs(self.position.y - other.position.y) + abs(
            self.position.z - other.position.z)


scanners: List[Scanner] = []

with open(input_filename) as fh:
    for line in (l.strip() for l in fh.readlines()):
        if not line:
            continue
        if match := re.search(r'scanner (\d+)', line):
            scanners.append(Scanner(match.group(1), None, None, []))
            continue

        scanners[-1].beacons.append(Vector(*map(int, line.split(','))))

scanners[0].position = Vector(0, 0, 0)

# scanners[0].beacons = [b for b in scanners[0].beacons if b.x == -618]
# scanners[1].beacons = [b for b in scanners[1].beacons if b.x == 686]
# scanners[0].position = Vector(0, 0, 0)
while any(s.position is None for s in scanners):
    for placed in (s for s in scanners if s.position):
        for candidate in (s for s in scanners if s.position is None):
            for t in transforms:
                counts = {}
                for v1, v2 in itertools.product(placed.beacons, candidate.beacons):
                    d = v1 - (t(v2))
                    counts.setdefault(d, 0)
                    counts[d] += 1
                    # print(f"{v1} - {v2} = {d}")
                for v, c in counts.items():
                    if c < 12:
                        continue
                    # FOUND POSITION
                    candidate.position = v
                    candidate.transform = t
                    candidate.beacons = [t(b) + v for b in candidate.beacons]
                if candidate.position:
                    break

unique_beacons = set()
for s in scanners:
    unique_beacons.update(s.beacons)

print("part1:", len(unique_beacons))

max_distance = 0
for s1, s2 in itertools.combinations(scanners, 2):
    max_distance = max(max_distance, s1.manhattan_distance(s2))

print("part2:", max_distance)

# d = Vector(68, -1246, -43)
# scanner = scanners[1]
# scanner.beacons = [b + d for b in scanner.beacons]
