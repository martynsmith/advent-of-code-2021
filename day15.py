#!/usr/bin/env python

# https://adventofcode.com/2021/day/15

from aoc import Vector, render_grid
import networkx as nx
from rich import print

input_filename = "input15.txt"
# input_filename = "sample15.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

deltas = [
    Vector(1, 0),
    Vector(0, 1),
]

grid = {}

for y, line in enumerate(data):
    for x, weight in enumerate(line):
        grid[Vector(x, y)] = int(weight)

G = nx.DiGraph()
for node in grid.keys():
    for d in deltas:
        if node + d in grid:
            G.add_edge(node, node + d, weight=grid[node + d])
            G.add_edge(node + d, node, weight=grid[node])

width = max(v.x for v in grid.keys()) + 1
height = max(v.y for v in grid.keys()) + 1

source = Vector(0, 0)
target = Vector(width - 1, height - 1)

print("part1:", nx.shortest_path_length(G, source=source, target=target, weight='weight'))


original_grid = list(grid.keys())
for y in range(0, 5):
    for x in range(0, 5):
        if x == 0 and y == 0:
            continue
        for node in original_grid:
            for d in deltas:
                if node + d in grid:
                    risk_delta = x + y
                    new_node = Vector(x * width + node.x, y * height + node.y)
                    grid[new_node] = (((grid[node] + risk_delta) % 9) or 9)

target = Vector(max(v.x for v in grid.keys()), max(v.y for v in grid.keys()))

G = nx.DiGraph()
for node in grid.keys():
    for d in deltas:
        if node + d in grid:
            G.add_edge(node, node + d, weight=grid[node + d])
            G.add_edge(node + d, node, weight=grid[node])

path = set(nx.shortest_path(G, source=source, target=target, weight='weight'))
# print(render_grid({
#     k: f"[red]{v}[/red]" if (k.x % 10) == 0 and (k.y % 10) == 0 else str(v) for k, v in grid.items()
# }))


print("part2:", nx.shortest_path_length(G, source=source, target=target, weight='weight'))
