from typing import NamedTuple, Dict

class Vector(NamedTuple):
    x: int
    y: int

    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)


def render_grid(grid: Dict[Vector, str], default_char=" "):
    min_x = min(v.x for v in grid.keys())
    min_y = min(v.y for v in grid.keys())
    max_x = max(v.x for v in grid.keys())
    max_y = max(v.y for v in grid.keys())

    out = ""

    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            out += grid.get(Vector(x, y), default_char)
        out += "\n"

    return out
