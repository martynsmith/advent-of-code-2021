#!/usr/bin/env python

# https://adventofcode.com/2021/day/17

from rich import print
import re
from aoc import Vector
import attr
from functools import reduce
from operator import add

input_filename = "input17.txt"
# input_filename = "sample17.txt"

with open(input_filename) as fh:
    min_x, max_x, min_y, max_y = map(int, re.findall(r'-?\d+', [l.strip() for l in fh.readlines()][0]))


@attr.define
class ValidY:
    steps: int
    final_y: int
    initial_dy: int
    peak: int


pos = Vector(0, 0)

valid_y = []

for initial_dy in range(min_y, -min_y):
    y = 0
    steps = 0
    dy = initial_dy
    peak = 0
    while y > max_y:
        y += dy
        peak = max(y, peak)
        steps += 1
        dy -= 1
    while y >= min_y:
        valid_y.append(ValidY(steps, y, initial_dy, peak))
        y += dy
        steps += 1
        dy -= 1

print("part1:", max(vy.peak for vy in valid_y))

initial_velocities = set()
for vy in valid_y:
    # if vy.initial_dy != -10:
    #     continue

    for potential_x in range(0, max_x+1):
        if potential_x < vy.steps:
            final_x = reduce(add, range(potential_x, 0, -1), 0)
        else:
            final_x = reduce(add, range(potential_x, potential_x - vy.steps, -1), 0)

        if min_x <= final_x <= max_x:
            # print(f"{potential_x},{vy.initial_dy}")
            initial_velocities.add(Vector(potential_x, vy.initial_dy))

print("part2:", len(initial_velocities))
