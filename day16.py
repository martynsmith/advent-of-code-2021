#!/usr/bin/env python

# https://adventofcode.com/2021/day/16
from functools import reduce

from rich import print
from collections import deque
from operator import mul

input_filename = "input16.txt"
# input_filename = "sample16.txt"

with open(input_filename) as fh:
    lines = [l.strip() for l in fh.readlines()]


def get_bits(queue, count):
    return "".join(queue.popleft() for i in range(count))


def get_bits_int(queue, count):
    return int("".join(queue.popleft() for i in range(count)), 2)


version_sum = 0


def parse(binary, packet_limit=None, bit_limit=None):
    global version_sum
    parsed_packets = 0
    values = []

    initial_stream_length = len(binary)
    while True:
        parsed_packets += 1
        version = int(get_bits(binary, 3), 2)
        version_sum += version

        type_id = get_bits_int(binary, 3)
        if type_id == 4:
            literal = ""
            while True:
                packet = get_bits(binary, 5)
                literal += packet[1:]
                if packet[0] == "0":
                    break
            literal = int(literal, 2)
            values.append(literal)
        else:
            length_type_id = get_bits(binary, 1)
            if length_type_id == "0":
                length = get_bits_int(binary, 15)
                result = parse(binary, bit_limit=length)
            else:
                length = get_bits_int(binary, 11)
                result = parse(binary, packet_limit=length)

            if type_id == 0:
                values.append(sum(result))
            elif type_id == 1:
                values.append(reduce(mul, result))
            elif type_id == 2:
                values.append(min(result))
            elif type_id == 3:
                values.append(max(result))
            elif type_id == 5:
                values.append(1 if result[0] > result[1] else 0)
            elif type_id == 6:
                values.append(1 if result[0] < result[1] else 0)
            elif type_id == 7:
                values.append(1 if result[0] == result[1] else 0)

        if packet_limit and parsed_packets >= packet_limit:
            break
        if bit_limit and initial_stream_length - len(binary) >= bit_limit:
            break

    return values


for line in lines:
    if line.startswith('#'):
        continue
    expected = None
    if ' == ' in line:
        line, expected = line.split(' == ')
        expected = int(expected)
    print(line)
    binary = deque("".join(bin(int(c, 16))[2:].zfill(4) for c in line))
    result = parse(binary, packet_limit=1)[0]
    if expected is not None:
        if result == expected:
            print(f'>[green] {result}')
        else:
            print(f'>[red] {result}')
    else:
        print('>', result)

print("part1:", version_sum)
