#!/usr/bin/env python

# https://adventofcode.com/2021/day/6

from rich import print
from collections import defaultdict

input_filename = "input6.txt"
# input_filename = "sample6.txt"

days = 256

with open(input_filename) as fh:
    start = [int(n) for n in [l.strip() for l in fh.readlines()][0].split(',')]

count_for_day = defaultdict(int)
total = 0

for n in start:
    total += 1

    d = n + 1
    while d <= days:
        count_for_day[d] += 1
        d += 7

for d in range(days+2):
    if d == 19:
        print("part1:", total)
    if d == 257:
        print("part2:", total)

    if d not in count_for_day:
        continue
    n = count_for_day[d]
    total += n
    d += 9
    while d <= days:
        count_for_day[d] += n
        d += 7
