#!/usr/bin/env python

# https://adventofcode.com/2021/day/9

from aoc import Vector
from rich import print
from math import prod

input_filename = "input9.txt"
# input_filename = "sample9.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

grid = {}

neighbour_deltas = [
    Vector(-1, 0),
    Vector(1, 0),
    Vector(0, -1),
    Vector(0, 1),
]

for y, line in enumerate(data):
    for x, n in enumerate(line):
        grid[Vector(int(x), int(y))] = int(n)

risk_level = 0
low_points = []
for pos, n in grid.items():
    if n < min([grid[pos + d] for d in neighbour_deltas if pos + d in grid]):
        risk_level += 1 + n
        low_points.append(pos)

print("part1:", risk_level)


def find_basin_size(points):
    new_points = points.copy()

    for p in points:
        new_points = new_points.union([p + d for d in neighbour_deltas if p + d in grid and grid[p + d] != 9])

    while new_points != points:
        points = new_points.copy()
        for p in points:
            new_points = new_points.union([p + d for d in neighbour_deltas if p + d in grid and grid[p + d] != 9])

    return len(new_points)


sizes = [find_basin_size(set([p])) for p in low_points]
print("part2:", prod(sorted(sizes)[-3:]))
